# PrimordialParticleSystems_Public

This repo holds source and  project files of implementations of Primordial Particle Systems, as described first by T. Schmickl & M. Stefanec in 2016. Everybody experimenting  with PPS is welcome to participate! Licensing of all code here: GLPL v3